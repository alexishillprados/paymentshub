<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Header -->
			<?php include_once("elements/header/primary.html") ?>

			<!-- Menu -->
			<div class="main_header secondary">
				<? $view = "configuration"; include_once("elements/header/secondary.html") ?>
			</div>

            <!-- Sub Menu -->
            <div class="main_header third menu_height">
                <? $subview = "clients_config"; include_once("elements/header/third.html") ?>
            </div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <article>Clients Configuration</article>
                            </div>

                        </div>
                    </div>




                    <!-- Content -->
                    <table>
                        <tr>
                            <th></th>
                            <th>Client ID</th>
                            <th>Name</th>
                            <th>External ID</th>
                            <th>Bank account</th>
                            <th>External URL</th>
                            <th>Experience service URL</th>
                            <th>Notification</th>
                            <th>Charge</th>
                        </tr>
                        <tr>
                            <td><span class="icon_20 arrow_tr toggle_detail_row" data-row="1"></span></td>
                            <td><span>0001</span></td>
                            <td><span>Cater Allen</span></td>
                            <td><span></span></td>
                            <td><span>11-22-33 12345678</span></td>
                            <td><span>www.google.com</span></td>
                            <td><span>www.google.com</span></td>
                            <td><span>FILE</span></td>
                            <td><span>BATCH</span></td>
                        </tr>
                        <!-- Detail -->
                        <tr class="detail_row hidden" id="1">
                            <td colspan="9" class="detail_tr_content">

                                <!-- Content detail tr -->
                                <div class="main_content_tr">
                                    <div class="wrap_content_tr">
                                        <div class="box_content_tr">

                                            <!-- Header tr -->
                                            <div class="header_content_tr">

                                                <!-- Left -->
                                                <div class="left_content">
                                                    <span class="info_text lh1_4">
                                                        <span class="title bolder">Client sort codes</span>
                                                    </span>
                                                </div>
                                            </div>

                                            <!-- Table content tr -->
                                            <table class="mt20">
                                                <tr>
                                                    <td>112233, 2233344, 334455, 445566, 556677</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td><span class="icon_20 arrow_tr toggle_detail_row" data-row="2"></span></td>
                            <td><span>0002</span></td>
                            <td><span>Modulr</span></td>
                            <td><span></span></td>
                            <td><span>11-22-33 12345678</span></td>
                            <td><span>www.google.com</span></td>
                            <td><span>www.google.com</span></td>
                            <td><span>ONLINE</span></td>
                            <td><span>ONLINE</span></td>
                        </tr>
                        <!-- Detail -->
                        <tr class="detail_row hidden" id="2">
                            <td colspan="9" class="detail_tr_content">

                                <!-- Content detail tr -->
                                <div class="main_content_tr">
                                    <div class="wrap_content_tr">
                                        <div class="box_content_tr">

                                            <!-- Header tr -->
                                            <div class="header_content_tr">

                                                <!-- Left -->
                                                <div class="left_content">
                                                    <span class="info_text lh1_4">
                                                        <span class="title bolder">Client sort codes</span>
                                                    </span>
                                                </div>
                                            </div>

                                            <!-- Table content tr -->
                                            <table class="mt20">
                                                <tr>
                                                    <td>112233, 2233344, 334455, 445566, 556677</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                    </table>


                    <!-- Footer -->
                    <div class="main_footer_content">
                        <div class="left_content"></div>
                        <div class="right_content">
                            <div class="main_paginator">
                                <span class="text"><b>1-3</b> of <b>3</b></span>
                                <span class="buttons_direction">
									<span class="btn secondary"><span class="icon_20 pag_left"></span></span>
									<span class="btn secondary"><span class="icon_20 pag_right"></span></span>
								</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>