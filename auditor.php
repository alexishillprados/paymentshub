<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Header -->
			<?php include_once("elements/header/primary.html") ?>

			<!-- Menu -->
			<div class="main_header secondary">
				<? $view = "auditor"; include_once("elements/header/secondary.html") ?>
			</div>

			<!-- Sub Menu -->
			<div class="main_header third menu_height">
				<? $subview = "audits_list"; include_once("elements/header/third.html") ?>
			</div>

			<div class="main_box_content">
				<div class="box_content pt0">

					<!-- Header -->
					<div class="main_header_content">
						<div class="wrap_header_content">

							<!-- Left -->
							<div class="left_content">
								<article>Audits list</article>
							</div>

							<!-- Right -->
							<div class="right_content">
								<div class="section_content">
									<a class="btn secondary icon_btn filter" data-toggle="modal" data-modal="filter-popup">Filters</a>
								</div>

								<div class="section_content">
									<a class="btn secondary icon_btn_only"><span class="icon_20 download"></span></a>
								</div>
							</div>
						</div>
					</div>



					<!-- Content -->
					<table>
						<tr>
							<th></th>
							<th>Instruction ID</th>
							<th>FPID</th>
							<th>Bank URN</th>
                            <th>Dir</th>
							<th>Status</th>
						</tr>
						<tr>
							<td><span class="icon_20 arrow_tr toggle_detail_row" data-row="1"></span></td>
							<td><span class="max_300">aCs324565lslk0900000as898aksl</span></td>
							<td><span>HUB908234098234098</span></td>
							<td><span>120938120398234</span></td>
                            <td><span class="icon_20 outbound center"></span></td>
							<td><span class="tag green">Accepted</span></td>
						</tr>

						<!-- Detail -->
						<tr class="detail_row hidden" id="1">
							<td colspan="6" class="detail_tr_content">

								<!-- Content detail tr -->
								<div class="main_content_tr">
									<div class="wrap_content_tr">
										<div class="box_content_tr">

											<!-- Header tr -->
											<div class="header_content_tr">

												<!-- Left -->
												<div class="left_content">

													<!-- Info date in -->
													<div class="wrap_info_date pr30">
														<span class="icon_position">
															<span class="icon_20 date start"></span>
														</span>
														<span class="info_text lh1_4">
															<span class="title bolder">Start time:</span>
															<span class="subtitle op6 f12">2017-02-09 <b>·</b> 09:42:06:005</span>
														</span>
													</div>

													<!-- Info date out -->
													<div class="wrap_info_date">
														<span class="icon_position">
															<span class="icon_20 date end"></span>
														</span>
														<span class="info_text lh1_4">
															<span class="title bolder">End time:</span>
															<span class="subtitle op6 f12">2017-02-09 <b>·</b> 09:42:06:005</span>
														</span>
													</div>
												</div>

												<!-- Right -->
												<div class="right_content lh1_4">
													<span class="block">Service status</span>
													<span class="block orange">Conditionaly accepted</span>
												</div>
											</div>

											<!-- Table content tr -->
											<table class="mt20">
												<tr>
													<th>Service name</th>
													<th>Excent. Category</th>
													<th>Excent. Message</th>
													<th>Inbound queue</th>
													<th>Outbound queue</th>
												</tr>
												<tr>
													<td>Outbound instruction emitter</td>
													<td>---</td>
													<td>---</td>
													<td>sink_forwarded Outbound Instruction</td>
													<td>source_emited Outbound Instruction</td>
												</tr>
											</table>

											<!-- Button content -->
											<div class="button_content_tr">
												<a class="btn secondary toggle_code_row">+ Show message content</a>
											</div>

											<!-- Code message 1 -->
											<div class="main_content_message_code_tr code_row hidden">

												<!-- Header -->
												<div class="wrap_header_message">
													<div class="left_content">
														<span class="title bolder">Message content</span>
													</div>
													<div class="right_content">
														<!-- <span class="copy_text"><span class="icon_20 tik_white"></span><span>Copied</span></span> -->
														<span class="icon_20 copy"></span>
														<!-- <span class="icon_20 minimize_message"></span> -->
														<span class="icon_20 toggle_code_row_item maximize_message"></span>
													</div>
												</div>

                                                <div class="main_content_message code_row_message">
                                                    <span>{1:F01CATEGB21XXXX0000000000}{2:I103RBOSGB2LXGPLN2020}{4:<br>:20:160216000141234<br>:23B:CRED<br>:32A:160216GBP-1944,00<br>:33B:GBP-1944,00<br>:50K:/16571053811234<br>AAAANTAGE LTD<br>RT AC 6666ROAD<br>AAAARD<br>TN17 4LZ<br>:59:/30929900362940<br>AAAA Kumar<br>:70:/RFB/RT AC 6666<br>:71A:OUR<br>:72:/FDP/<br>-}</span>
                                                </div>

                                                <!-- Code message 2 -->
                                                <div class="main_content_message_code_tr code_row hidden">

                                                    <!-- Header -->
                                                    <div class="wrap_header_message">
                                                        <div class="left_content">
                                                            <span class="title bolder">Original Message content</span>
                                                        </div>
                                                        <div class="right_content">
                                                            <span class="copy_text"><span class="icon_20 tik_white"></span><span>Copied</span></span>
                                                            <span class="icon_20 copy"></span>
                                                            <!-- <span class="icon_20 minimize_message"></span> -->
                                                            <span class="icon_20 toggle_code_row_item maximize_message"></span>
                                                        </div>
                                                    </div>

                                                    <div class="main_content_message code_row_message">
                                                        <span>{1:F01CATEGB21XXXX0000000000}{2:I103RBOSGB2LXGPLN2020}{4:<br>:20:160216000141234<br>:23B:CRED<br>:32A:160216GBP-1944,00<br>:33B:GBP-1944,00<br>:50K:/16571053811234<br>AAAANTAGE LTD<br>RT AC 6666ROAD<br>AAAARD<br>TN17 4LZ<br>:59:/30929900362940<br>AAAA Kumar<br>:70:/RFB/RT AC 6666<br>:71A:OUR<br>:72:/FDP/<br>-}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td><span class="icon_20 arrow_tr"></span></td>
							<td><span class="max_300">Cs324565lslk0900000as898aksl</span></td>
							<td><span>HUB908234098234098</span></td>
							<td><span>120938120398234</span></td>
                            <td><span class="icon_20 outbound center"></span></td>
							<td><span class="tag green">Conditionally accepted</span></td>
						</tr>
					</table>

                    <!-- Footer -->
                    <div class="main_footer_content">
                        <div class="left_content"></div>
                        <div class="right_content">
                            <div class="main_paginator">
                                <span class="text"><b>1-3</b> of <b>3</b></span>
                                <span class="buttons_direction">
									<span class="btn secondary"><span class="icon_20 pag_left"></span></span>
									<span class="btn secondary"><span class="icon_20 pag_right"></span></span>
								</span>
                            </div>
                        </div>
                    </div>
				</div>
			</div>

			<!-- Footer -->
			<?php include("elements/footer.html") ?>
		</div>

		<!-- No responsive -->
		<div class="main_global_structure_no_responsive">
			<?php include("elements/no-responsive.html") ?>
		</div>
	</body>
</html>



