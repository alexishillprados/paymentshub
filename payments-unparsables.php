<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Header -->
			<?php include_once("elements/header/primary.html") ?>

			<!-- Menu -->
			<div class="main_header secondary">
				<? $view = "payments"; include_once("elements/header/secondary.html") ?>
			</div>

			<!-- Sub Menu -->
			<div class="main_header third menu_height">
				<? $subview = "unparsables"; include_once("elements/header/third.html") ?>
			</div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <article>Unparseables</article>
                            </div>

                            <!-- Right -->
                            <div class="right_content">
                                <div class="section_content">
                                    <a class="btn secondary icon_btn filter" data-toggle="modal" data-modal="filter-popup">Filters</a>
                                </div>

                                <div class="section_content">
                                    <a class="btn secondary icon_btn_only"><span class="icon_20 download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>



                    <!-- Content -->
                    <table>
                        <tr>
                            <th></th>
                            <th>PaymentHub ID</th>
                            <th>Client ID</th>
                            <th>Timestamp</th>
                            <th>Source name</th>
                            <th>Dir</th>
                        </tr>
                        <tr>
                            <td><span class="icon_20 arrow_tr toggle_detail_row" data-row="1"></span></td>
                            <td><span>HUBX1212121q235242345234678657885671</span></td>
                            <td><span>Cater Allen</span></td>
                            <td><span>10-10-2017T00:10:20.300</span></td>
                            <td><span>MT103</span></td>
                            <td><span class="icon_20 outbound center"></span></td>
                        </tr>
                        <!-- Detail -->
                        <tr class="detail_row hidden" id="1">
                            <td colspan="6" class="detail_tr_content">

                                <!-- Content detail tr -->
                                <div class="main_content_tr">
                                    <div class="wrap_content_tr">
                                        <div class="box_content_tr">
                                            <!-- Code message 1 -->
                                            <div class="main_content_message_code_tr code_row">

                                                <!-- Header -->
                                                <div class="wrap_header_message">
                                                    <div class="left_content">
                                                        <span class="title bolder">Message content</span>
                                                    </div>
                                                    <div class="right_content">
                                                        <!-- <span class="copy_text"><span class="icon_20 tik_white"></span><span>Copied</span></span> -->
                                                        <span class="icon_20 copy"></span>
                                                        <!-- <span class="icon_20 minimize_message"></span> -->
                                                        <span class="icon_20 toggle_code_row_item maximize_message"></span>
                                                    </div>
                                                </div>
                                                <div class="main_content_message code_row_message">
                                                    <span>{1:F01CATEGB21XXXX0000000000}{2:I103RBOSGB2LXGPLN2020}{4:<br>:20:160216000141234<br>:23B:CRED<br>:32A:160216GBP-1944,00<br>:33B:GBP-1944,00<br>:50K:/16571053811234<br>AAAANTAGE LTD<br>RT AC 6666ROAD<br>AAAARD<br>TN17 4LZ<br>:59:/30929900362940<br>AAAA Kumar<br>:70:/RFB/RT AC 6666<br>:71A:OUR<br>:72:/FDP/<br>-}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td><span class="icon_20 arrow_tr toggle_detail_row" data-row="2"></span></td>
                            <td><span>HUBX1212121q235242345234678657885671</span></td>
                            <td><span>Cater Allen</span></td>
                            <td><span>10-10-2017T00:10:20.300</span></td>
                            <td><span>MT103</span></td>
                            <td><span class="icon_20 outbound center"></span></td>
                        </tr>
                        <!-- Detail -->
                        <tr class="detail_row hidden" id="2">
                            <td colspan="6" class="detail_tr_content">

                                <!-- Content detail tr -->
                                <div class="main_content_tr">
                                    <div class="wrap_content_tr">
                                        <div class="box_content_tr">
                                            <!-- Code message 1 -->
                                            <div class="main_content_message_code_tr code_row">

                                                <!-- Header -->
                                                <div class="wrap_header_message">
                                                    <div class="left_content">
                                                        <span class="title bolder">Message content</span>
                                                    </div>
                                                    <div class="right_content">
                                                        <!-- <span class="copy_text"><span class="icon_20 tik_white"></span><span>Copied</span></span> -->
                                                        <span class="icon_20 copy"></span>
                                                        <!-- <span class="icon_20 minimize_message"></span> -->
                                                        <span class="icon_20 toggle_code_row_item maximize_message"></span>
                                                    </div>
                                                </div>
                                                <div class="main_content_message code_row_message">
                                                    <span>{1:F01CATEGB21XXXX0000000000}{2:I103RBOSGB2LXGPLN2020}{4:<br>:20:160216000141234<br>:23B:CRED<br>:32A:160216GBP-1944,00<br>:33B:GBP-1944,00<br>:50K:/16571053811234<br>AAAANTAGE LTD<br>RT AC 6666ROAD<br>AAAARD<br>TN17 4LZ<br>:59:/30929900362940<br>AAAA Kumar<br>:70:/RFB/RT AC 6666<br>:71A:OUR<br>:72:/FDP/<br>-}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                    </table>


                    <!-- Footer -->
                    <div class="main_footer_content">
                        <div class="left_content"></div>
                        <div class="right_content">
                            <div class="main_paginator">
                                <span class="text"><b>1-3</b> of <b>3</b></span>
                                <span class="buttons_direction">
									<span class="btn secondary"><span class="icon_20 pag_left"></span></span>
									<span class="btn secondary"><span class="icon_20 pag_right"></span></span>
								</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>