<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Header -->
			<?php include_once("elements/header/primary.html") ?>

			<!-- Menu -->
			<div class="main_header secondary menu_height">
				<?php $view = "home"; include_once("elements/header/secondary.html") ?>
			</div>

			<!-- Content -->
			<div class="main_box_content dashboard">
				<div class="box_content">
					
					<!-- Header -->
					<div class="main_header_dashboard">
						
						<!-- Left -->
						<div class="left_content">
							
							<!-- Title -->
							<div class="main_title" tabindex="0">
								<span class="title">All clients</span>
								<span class="icon_20 arrow_white"></span>
								
								<!-- Popover -->
								<div class="wrap_popover dark">
									<ul class="main_popover_options">
										<li>Allclients <span class="icon_20 tik_white"></span></li>
										<span class="sep"></span>
										<li>Modulr</li>
									</ul>
								</div>
							</div>

						</div>
						
						<!-- Middle -->
						<div class="middle_content">
							<a href="javascript:;" class="select">Today</a>
							<a href="javascript:;">7 days</a>
							<a href="javascript:;">Month</a>
							<a href="javascript:;">3 Months</a>
						</div>

						<!-- Right -->
						<div class="right_content">
							<a href="javascript:;">Filters</a>
						</div>
					</div>



					<!-- Content -->
					<div class="main_content_dashboard">

						<!-- Left content-->
						<div class="left_content">
							
							<!-- PaymentsHub -->
							<div class="main_box_content_payments">

								<!-- Title -->
								<div class="title_box_payments pb20">
									<span class="f14 op5 block pb5 pt10">Today <b>·</b> Mon. 02 Nov. 2017</span>
									<article>PaymentsHub</article>
								</div>

								<div class="wrap_box_content_payments">

									<!-- Outbound -->
									<div class="main_info pr20">
										
										<!-- Title -->
										<div class="title_main_info blue_2 pb20">
											<span class="title">Outbound</span>
										</div>

										<!-- 1 -->
										<div class="wrap_info">
											<span class="f14"><b class="f18 bold">4</b> <b class="normal op5 f16">accepted</b></span>
											<span class="price blue_2">£50.04</span>
											<span class="op5 f18 bolder">£45.07 <b class="f12">(+7%)</b></span>
										</div>

										<!-- 2 -->
										<div class="wrap_info">
											<span class="f14"><b class="f18 bold">0</b> <b class="normal op5 f16">rejected</b></span>
											<span class="price blue_2">£0.04</span>
											<span class="op5 f18 bolder">£1.05 <b class="f12">(+1%)</b></span>
										</div>

                                        <!-- 3 -->
                                        <div class="wrap_info">
                                            <span class="f14"><b class="f18 bold">1</b> <b class="error op5 f16">pending</b></span>
                                            <span class="op5 f18 bolder">£0.04</span>
                                        </div>
									</div>

									<!-- Inbound -->
									<div class="main_info pl20">
										
										<!-- Title -->
										<div class="title_main_info green_2 pb20">
											<span class="title">Inbound</span>
										</div>

										<!-- 1 -->
										<div class="wrap_info">
											<span class="f14"><b class="f18 bold">18</b> <b class="normal op5 f16">accepted</b></span>
											<span class="price green_2">£3.575.10</span>
											<span class="op5 f18 bolder">£2.875.07 <b class="f12">(+7%)</b></span>
										</div>

										<!-- 2 -->
										<div class="wrap_info">
											<span class="f14"><b class="f18 bold">2</b> <b class="normal op5 f16">rejected</b></span>
											<span class="price green_2">£20.31</span>
											<span class="op5 f18 bolder">£19.00 <b class="f12">(+2%)</b></span>
										</div>

                                        <!-- 3 -->
                                        <div class="wrap_info">
                                            <span class="f14"><b class="f18 bold">0</b> <b class="normal op5 f16">pending</b></span>
                                            <span class="op5 f18 bolder">£0.00</span>
                                        </div>
									</div>
								</div>


								<!-- Box stat -->
								<div class="main_box_stat_payments">
									<img src="images/home/stat-big.png">
								</div>







							</div>
						</div>

						<!-- Right content -->
						<div class="right_content">
							
							<!-- Batch -->
							<div class="main_box_content">

								<!-- Title -->
								<article>Batch <span class="right f14 op5 pt5">+8%</span></article>

								<div class="main_info_content">

									<!-- Outbound -->
									<div class="main_info">

										<!-- Title -->
										<div class="title_main_info blue_2">
											<span class="title">Outbound</span>
										</div>

										<!-- 1 -->
										<div class="wrap_info">
											<span class="f14"><b class="f16 bold">4</b> <b class="normal op5">accepted</b></span>
											<span class="price blue_2">£50.04</span>
											<span class="op5">£45.07 (+7%)</span>
										</div>

										<!-- 2 -->
										<div class="wrap_info">
											<span class="f14"><b class="f16 bold">0</b> <b class="normal op5">rejected</b></span>
											<span class="price blue_2">£0.04</span>
											<span class="op5">£1.05 (+1%)</span>
										</div>

                                        <!-- 3 -->
                                        <div class="wrap_info">
                                            <span class="f14"><b class="f16 bold">1</b> <b class="normal op5">pending</b></span>
                                            <span class="op5">£0.04</span>
                                        </div>
									</div>

									<!-- Inbound -->
									<div class="main_info">
										
										<!-- Title -->
										<div class="title_main_info green_2">
											<span class="title">Inbound</span>
										</div>

										<!-- 1 -->
										<div class="wrap_info">
											<span class="f14"><b class="f16 bold">11</b> <b class="normal op5">accepted</b></span>
											<span class="price green_2">£230.01</span>
											<span class="op5">£228.04 (+2%)</span>
										</div>

										<!-- 2 -->
										<div class="wrap_info">
											<span class="f14"><b class="f16 bold">0</b> <b class="normal op5">rejected</b></span>
											<span class="price green_2">£0.00</span>
                                            <span class="op5">£0.00 (+0%)</span>
										</div>

                                        <!-- 3 -->
                                        <div class="wrap_info">
                                            <span class="f14"><b class="f16 bold">0</b> <b class="normal op5">pending</b></span>
                                            <span class="op5">£0.00</span>
                                        </div>
									</div>
								</div>
							</div>

							<!-- API -->
							<div class="main_box_content">
								
								<!-- Title -->
								<article>API <span class="right f14 op5 pt5">+1%</span></article>

								<div class="main_info_content">
									<!-- Outbound -->
									<div class="main_info">

										<!-- Title -->
										<div class="title_main_info blue_2">
											<span class="title">Outbound</span>
										</div>

										<!-- 1 -->
										<div class="wrap_info">
											<span class="f14"><b class="f16 bold">18</b> <b class="normal op5">accepted</b></span>
											<span class="price blue_2">£30.08</span>
											<span class="op5">£1.07 (+1%)</span>
										</div>

										<!-- 2 -->
										<div class="wrap_info">
											<span class="f14"><b class="f16 bold">0</b> <b class="normal op5">rejected</b></span>
											<span class="price blue_2">£0.04</span>
											<span class="op5">£1.05 (+1%)</span>
										</div>

                                        <!-- 3 -->
                                        <div class="wrap_info">
                                            <span class="f14"><b class="f16 bold">0</b> <b class="normal op5">pending</b></span>
                                            <span class="op5">£0.00</span>
                                        </div>
									</div>

									<!-- Inbound -->
									<div class="main_info">
										
										<!-- Title -->
										<div class="title_main_info green_2">
											<span class="title">Inbound</span>
										</div>

										<!-- 1 -->
										<div class="wrap_info">
											<span class="f14"><b class="f16 bold">7</b> <b class="normal op5">accepted</b></span>
											<span class="price green_2">£3.345.09</span>
											<span class="op5">£3.234,04 (+2%)</span>
										</div>

										<!-- 2 -->
										<div class="wrap_info">
											<span class="f14"><b class="f16 bold">2</b> <b class="normal op5">rejected</b></span>
											<span class="price green_2">£20.01</span>
											<span class="op5">£20.04 (+0.4%)</span>
										</div>

                                        <!-- 3 -->
                                        <div class="wrap_info">
                                            <span class="f14"><b class="f16 bold">0</b> <b class="normal op5">penging</b></span>
                                            <span class="op5">£0.00</span>
                                        </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Footer -->
			<?php include("elements/footer.html") ?>	
		</div>

		<!-- No responsive -->
		<div class="main_global_structure_no_responsive">
			<?php include("elements/no-responsive.html") ?>
		</div>
	</body>
</html>