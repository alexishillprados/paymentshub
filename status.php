<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

    <div class="main_global_structure">

        <!-- Header -->
        <?php include_once("elements/header/primary.html") ?>

        <!-- Menu -->
        <div class="main_header secondary">
            <? $view = "status"; include_once("elements/header/secondary.html") ?>
        </div>

        <!-- Sub Menu -->
        <div class="main_header third menu_height">
            <? $subview = "fps_status"; include_once("elements/header/third.html") ?>
        </div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <article>Faster payment status</article>
                            </div>

                        </div>
                    </div>

                    <!-- Content -->
                    <table>
                        <tr>
                            <th>Last update timestamp</th>
                            <th>Last update message id</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <td><span>10-10-2017T00:10:20.300</span></td>
                            <td><span>10-10-2017T00:10:20.300</span></td>
                            <td><span class="tag green">Active</span></td>
                        </tr>
                    </table>

                </div>
            </div>

            <!-- Footer -->
            <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>