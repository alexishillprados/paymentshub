<!DOCTYPE html>
<html>
	<head>
		<title>Theme</title>
		<?php include_once("sections/meta.html") ?>
	</head>
	<body>

		<div class="wrap-theme">
			<div class="content-theme">
			
				<hr>

				<!-- Textinput -->
				<div>
					<h3>Textinput</h3>
					<input type="text" placeholder="Textinput normal status">
					<input type="text" class="err" placeholder="Textinput error">
					<input type="text" class="dis" placeholder="Textinput disabled" disabled>
				</div>

				<hr>

				<!-- Textarea -->
				<div>
					<h3>Textarea</h3>
					<textarea placeholder="Textarea normal status"></textarea>
					<textarea class="err" placeholder="Textarea error"></textarea>
					<textarea class="dis" placeholder="Textarea disabled" disabled></textarea>
				</div>

				<hr>

				<!-- Button -->
				<div>
					<h3>Buttons</h3>
					<a href="" class="primary dis">Disabled</a>
					<a href="" class="primary">Primary</a>
					<a href="" class="secondary">Secondary</a>
				</div>

				<hr>

				<!-- Combo -->
				<div>
					<h3>Combo Box · Select</h3>
					<span class="arrows-combo">
						<select>
							<option style="display:none;">Select option</option>
							<option>Option 1</option>
							<option>Option 2</option>
							<option>Option 3</option>
						</select>
					</span>

					<span class="arrows-combo">
						<select class="err">
							<option style="display:none;">Select option</option>
							<option>Option 1</option>
							<option>Option 2</option>
							<option>Option 3</option>
						</select>
					</span>

					<span class="arrows-combo">
						<select class="dis" disabled>
							<option style="display:none;">Select option</option>
							<option>Option 1</option>
							<option>Option 2</option>
							<option>Option 3</option>
						</select>
					</span>
				</div>

				<hr>

				<!-- Radio -->
				<div>
					<h3>RadioButton</h3>
					<div class="radio"><input type="radio" name="group1" id="radio" value="Mujer" checked><label for="radio">Man</label></div>
					<div class="radio err"><input type="radio" name="group1" id="radio1" value="Mujer"><label for="radio1">Woman</label></div>
					<div class="radio dis"><input type="radio" name="group2" id="radio2" value="Mujer" disabled><label for="radio2">Trans</label></div>
				</div>

				<hr>

				<!-- Check -->
				<div>
					<h3>CheckBox</h3>
					<div class="check"><input type="checkbox" value="" id="check"><label for="check">CheckBox</label></div>
					<div class="check err"><input type="checkbox" value="" id="check2"><label for="check2">CheckBox</label></div>
					<div class="check dis"><input type="checkbox" value="" id="check1" disabled><label for="check1">CheckBox</label></div>
				</div>

				<hr>

				<!-- Link -->

				<div>
					<a href="">Link default</a>
				</div>

			</div>
		</div>
		
	</body>
</html>