<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Content login -->
			<div class="main_login_page">
				<div class="wrap_login">
					<div class="main_content_login">

						<!-- Logo -->
						<span class="logo_ph">
							<img src="images/logo-ph.png">
						</span>

						<!-- Title text -->
						<p>Enter your email where we will send you instructions on how to recover access.</p>

						<!-- Form -->
						<div class="form">
							<input type="mail" placeholder="Username" class="mb10">
							<a href="" class="btn primary">Recover</a>
						</div>

						<!-- Footer -->
						<div class="footer">
							<span class="text">Remember?</span>
							<a href="login.php">Go to Login!</a>
						</div>
					</div>

					<!-- By Santander -->
					<div class="by_santander">
						<img src="images/by-santander.png">
					</div>
				</div>
			</div>
		</div>

		<!-- No responsive -->
		<div class="main_global_structure_no_responsive">
			<?php include("elements/no-responsive.html") ?>
		</div>
	</body>
</html>



