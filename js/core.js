$(function() {
	popoverListener();
	lateralListener();

	modalListener();
	tooltip();

	filtersListener();

	rowListener();
});





var filtersListener = function() {
	$("#toggle_filter_extra").click(function() {
		$(this).closest("div").hide();
		$("#filter_extra").removeClass("hidden");
	});
};






/* Modal Listener */
var modalListener = function() {
	$("[data-toggle=modal]").click(function() {
		id_modal = $(this).data("modal");
		$("#"+id_modal).css("display", "table");
		document.body.style.overflow = "hidden";
	});

	$(".close-modal").click(function() {
		closeModal();
	});

	$(".content_modal").click(function() {
		closeModal();
	});

	$(".main_popup, .main_confirm, .main_preview_photo_profile").click(function(e) {
		e.stopPropagation();
	});

	$(".main_popup_wall").click(function(e) {
		e.stopPropagation();
	});

	$(document).keyup(function(e) {
		k = e.keyCode;

		if(k == 27) {
			closeModal();
		}
	});


	function closeModal(){
		document.body.style.overflow = "visible";
		$(".main_modal").each(function() {
			$(this).fadeOut(100);
		});
	}
};



/**
 * Shared functions for lateral detail panel
 */
var lateralListener = function() {

	// Toggle lateral from table row
	$(".toggle_lateral_item").click(function(e){
		e.stopPropagation();

		ref = $(this).data("ref");

		$(".toggle_lateral_item[data-ref='"+ref+"'].select").removeClass('select');
		$(this).addClass('select');

		$(".main_lateral_popup[id='"+ref+"'").show();
		$(".main_lateral_popup[id='"+ref+"'").animate({
			right: 0
		}, 300, function() {
			$(".main_lateral_popup[id='"+ref+"'").removeClass('invisible');
		});
	});


	// Prevent lateral close on inside click
	$(".main_lateral_popup").click(function(e) {
		e.stopPropagation();
	});


	$(".close_lateral").click(function() {
		closeLateral();
	});


	// Events for lateral close
	$(document).click(function() {
		closeLateral();
	}).keyup(function(e) {
		if(e.which == 27)
			closeLateral();
	});


	// Lateral toggle close
	function closeLateral() {
		if($(".main_lateral_popup").not(".invisible").length > 0) {
			item = $(".main_lateral_popup").not(".invisible");
			$(".main_lateral_popup").not(".invisible").animate({
				right: -1200
			}, 100, function() {
				item.addClass('invisible');
				item.hide();
			});
			$(".toggle_lateral_item.select").removeClass('select');
		}
	}

}












/**
 * Shared functions for popover
 */
var popoverListener = function() {
	// Popover toggle
	$(".popover_toggle").click(function(e) {
		e.stopPropagation();

		// Close other open popovers
		if($(".popover_active").not($(this)).length > 0) {
			$(".popover_active").blur();
			$(".popover_active").find(".popover_item").removeClass('view');
			$(".popover_active").removeClass('popover_active');
		}

		$(this).toggleClass('popover_active');

		if($(this).hasClass('popover_active')) {
			$(this).find(".popover_item").addClass('view');

			if($(this).find(".popover_search").length > 0)
				$(this).find(".popover_search").focus();

		}else {
			$(this).blur();
			$(this).find(".popover_item").removeClass('view');
		}
	});


	// Prevent popover close on inside click
	$(".popover_item, .popover_search").click(function(e) {
		e.stopPropagation();
	});


	// Events for popover close
	$(document).click(function() {
		closePopover();
	}).keyup(function(e) {
		if(e.which == 27)
			closePopover();
	});


	// Popover toggle close
	function closePopover() {
		if($(".popover_active").length > 0) {
			$(".popover_active").click();
		}
	}
}




// Mejora de rendimiento
document.addEventListener("touchstart", function(){}, true)




/**
* Tooltipster
*/
var tooltip = function() {
	$('[data-toggle="tooltip"], .tooltip').tooltipster({
    	contentAsHTML: true,
    	multiple: true
    });

    $('[data-toggle="tooltip-bottom"], .tooltip-bottom').tooltipster({
    	contentAsHTML: true,
    	multiple: true,
    	position: "bottom"
    });

    $('[data-toggle="tooltip-top"], .tooltip-top').tooltipster({
    	contentAsHTML: true,
    	multiple: true,
    	position: "top"
    });

    $('[data-toggle="tooltip-left"], .tooltip-left').tooltipster({
    	contentAsHTML: true,
    	multiple: true,
    	position: "left"
    });

    $('[data-toggle="tooltip-right"], .tooltip-right').tooltipster({
    	contentAsHTML: true,
    	multiple: true,
    	position: "right"
    });

    $('[data-toggle="tooltip-light"], .tooltip-light').tooltipster({
    	theme: 'light',
    	contentAsHTML: true,
    	multiple: true
    });

    $('[data-toggle="tooltip-light-bottom"], .tooltip-light-bottom').tooltipster({
    	theme: 'light',
    	contentAsHTML: true,
    	multiple: true,
    	position: "bottom"
    });

    $('[data-toggle="tooltip-light-left"], .tooltip-light-left').tooltipster({
    	theme: 'light',
    	contentAsHTML: true,
    	multiple: true,
    	position: "left"
    });

    $('[data-toggle="tooltip-light-right"], .tooltip-light-right').tooltipster({
    	theme: 'light',
    	contentAsHTML: true,
    	multiple: true,
    	position: "right"
    });
};






/**
 * Listener for table rows and details
 */
var rowListener = function() {

	$(".toggle_detail_row").click(function() {
		$(this).toggleClass('open');

		rowId = $(this).data("row");

		$(".detail_row[id='"+rowId+"']").toggleClass('hidden');

	});




	$(".toggle_code_row").click(function() {
		if($(this).hasClass('open')) {
			// Hide
			$(this).removeClass('open');
			$(this).html("+ Show message content");

			$(this).closest(".detail_row").find(".code_row").addClass('hidden');
		}else {
			// Show
			$(this).addClass('open');
			$(this).html("- Hide message content");

			$(this).closest(".detail_row").find(".code_row").removeClass('hidden');
		}
	});



	$(".toggle_code_row_item").click(function() {
		if($(this).hasClass('maximize_message')) {
			$(this).closest(".code_row").find(".code_row_message").addClass('hidden');
			$(this).addClass('minimize_message');
			$(this).removeClass('maximize_message');
		}else {
			$(this).closest(".code_row").find(".code_row_message").removeClass('hidden');
			$(this).removeClass('minimize_message');
			$(this).addClass('maximize_message');
		}
	});

};







