<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Header -->
			<?php include_once("elements/header/primary.html") ?>

			<!-- Menu -->
			<div class="main_header secondary">
				<? $view = "payments"; include_once("elements/header/secondary.html") ?>
			</div>

			<!-- Sub Menu -->
			<div class="main_header third menu_height">
				<? $subview = "payments_list"; include_once("elements/header/third.html") ?>
			</div>

			<!-- Content -->
			<div class="main_box_content">
				<div class="box_content pt0">

					<!-- Header -->
					<div class="main_header_content">
						<div class="wrap_header_content">

							<!-- Left -->
							<div class="left_content">
								<article>Payments lists</article>
							</div>

							<!-- Right -->
							<div class="right_content">
                                <div class="section_content">
									<a class="btn secondary icon_btn filter" data-toggle="modal" data-modal="filter-popup">Filters</a>
								</div>

								<div class="section_content">
									<a class="btn secondary icon_btn_only"><span class="icon_20 download"></span></a>
								</div>
                            </div>
						</div>
						



						<!-- Filter -->
						<div class="main_section_filter">

							<span class="close_filter"></span>
							<span class="tag">
								<span class="text"><b>Payment scheme:</b> Faster Payments</span>
								<span class="delete"></span>
							</span>
							<span class="tag">
								<span class="text"><b>Direction:</b> Inbound</span>
								<span class="delete"></span>
							</span>
						</div>
					</div>




					<!-- Content -->
					<table>
						<tr>
                            <th>Client</th>
                            <th>Date</th>
                            <th>FPID</th>
                            <th>Originating Account</th>
							<th>Destination Account</th>
                            <th class>Amount</th>
							<th>Dir</th>
                            <th>Status</th>
						</tr>
						<tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>Cater Allen</span></td>
                            <td><span>10-10-2017</span></td>
                            <td><span>HUBX1212121..</span></td>
							<td><span>11-22-33 12345678</span></td>
							<td><span>99-88-77 87654321</span></td>
                            <td class="right"><span>£671,00</span></td>
                            <td><span class="icon_20 outbound center"></span></td>
                            <td><span class="tag green">Accepted</span></td>
						</tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>Cater Allen</span></td>
                            <td><span>10-11-2017</span></td>
                            <td><span>HUBX1212121..</span></td>
                            <td><span>11-22-33 12345678</span></td>
                            <td><span>99-88-77 87654321</span></td>
                            <td class="right"><span>£2.310,50</span></td>
                            <td><span class="icon_20 inbound"></span></td>
                            <td><span class="tag green">Conditionally Accepted</span></td>
                        </tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>Modulr</span></td>
                            <td><span>10-10-2017</span></td>
                            <td><span>HUBX1212121..</span></td>
                            <td><span>11-22-33 12345678</span></td>
                            <td><span>99-88-77 87654321</span></td>
                            <td class="right"><span>£671,00</span></td>
                            <td><span class="icon_20 outbound"></span></td>
                            <td><span class="tag green">Reversed</span></td>
                        </tr>
						<tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>Modulr</span></td>
                            <td><span>10-11-2017</span></td>
							<td><span>HUBX1212121..</span></td>
                            <td><span>11-22-33 12345678</span></td>
							<td><span>99-88-77 87654321</span></td>
                            <td class="right"><span>£42.620,00</span></td>
                            <td><span class="icon_20 outbound"></span></td>
                            <td><span class="tag yellow">Invalid</span></td>
						</tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>Modulr</span></td>
                            <td><span>10-11-2017</span></td>
                            <td><span>HUBX1212121..</span></td>
                            <td><span>11-22-33 12345678</span></td>
                            <td><span>99-88-77 87654321</span></td>
                            <td class="right"><span>£2.310,50</span></td>
                            <td><span class="icon_20 outbound"></span></td>
                            <td><span class="tag orange">Rejected by FPC</span></td>
                        </tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>Modulr</span></td>
                            <td><span>10-11-2017</span></td>
                            <td><span>HUBX1212121..</span></td>
                            <td><span>11-22-33 12345678</span></td>
                            <td><span>99-88-77 87654321</span></td>
                            <td class="right"><span>£2.310,50</span></td>
                            <td><span class="icon_20 inbound"></span></td>
                            <td><span class="tag orange">Rejected</span></td>
                        </tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>Retail</span></td>
                            <td><span>10-11-2017</span></td>
                            <td><span>HUBX1212121..</span></td>
                            <td><span>11-22-33 12345678</span></td>
                            <td><span>99-88-77 87654321</span></td>
                            <td class="right"><span>£65.234,11</span></td>
                            <td><span class="icon_20 inbound"></span></td>
                            <td><span class="tag red">Overlimit</span></td>
                        </tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>Corporate</span></td>
                            <td><span>10-11-2017</span></td>
                            <td><span>HUBX1212121..</span></td>
                            <td><span>11-22-33 12345678</span></td>
                            <td><span>99-88-77 87654321</span></td>
                            <td class="right"><span>£2.310,50</span></td>
                            <td><span class="icon_20 inbound"></span></td>
                            <td><span class="tag red_error">Pending</span></td>
                        </tr>
					</table>


					<!-- Footer -->
					<div class="main_footer_content">
						<div class="left_content"></div>
						<div class="right_content">
							<div class="main_paginator">
								<span class="text"><b>1-3</b> of <b>3</b></span>
								<span class="buttons_direction">
									<span class="btn secondary"><span class="icon_20 pag_left"></span></span>
									<span class="btn secondary"><span class="icon_20 pag_right"></span></span>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Footer -->
			<?php include("elements/footer.html") ?>
		</div>

		<!-- Lateral popup detail -->
		<?php include("elements/lateral-popup/payments/detail.html") ?>

		<!-- Popup Filter -->
		<?php include("elements/popups/popup-filter.html") ?>


		<!-- No responsive -->
		<div class="main_global_structure_no_responsive">
			<?php include("elements/no-responsive.html") ?>
		</div>
	</body>
</html>