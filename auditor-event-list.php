<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

            <!-- Header -->
            <?php include_once("elements/header/primary.html") ?>

            <!-- Menu -->
            <div class="main_header secondary">
                <? $view = "auditor"; include_once("elements/header/secondary.html") ?>
            </div>

            <!-- Sub Menu -->
            <div class="main_header third menu_height">
                <? $subview = "event_list"; include_once("elements/header/third.html") ?>
            </div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <article>Events list</article>
                            </div>

                            <!-- Right -->
                            <div class="right_content">
                                <div class="section_content">
                                    <a class="btn secondary icon_btn filter" data-toggle="modal" data-modal="filter-popup">Filters</a>
                                </div>

                                <div class="section_content">
                                    <a class="btn secondary icon_btn_only"><span class="icon_20 download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Content -->
                    <table>
                        <tr>
                            <th>Timestamp</th>
                            <th>User</th>
                            <th>Event type</th>
                            <th>Description</th>
                            <th>Operation</th>
                            <th>Stage</th>
                        </tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>10-10-2017T00:10:20.300</span></td>
                            <td><span>system</span></td>
                            <td><span>SFTP</span></td>
                            <td><span>MT198</span></td>
                            <td><span>Consumption</span></td>
                            <td><span class="tag green">Finished</span></td>
                        </tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>10-10-2017T00:10:20.300</span></td>
                            <td><span>C0249407</span></td>
                            <td><span>SFTP</span></td>
                            <td><span>MT103</span></td>
                            <td><span>Generation</span></td>
                            <td><span class="tag red_error">Crashed</span></td>
                        </tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>10-10-2017T00:10:20.300</span></td>
                            <td><span>system</span></td>
                            <td><span>Manual request</span></td>
                            <td><span>MT198</span></td>
                            <td><span>Consumption</span></td>
                            <td><span class="tag green">Started</span></td>
                        </tr>
                        <tr data-ref="payments_detail_lateral" class="toggle_lateral_item hover">
                            <td><span>10-10-2017T00:10:20.300</span></td>
                            <td><span>system</span></td>
                            <td><span>SFTP</span></td>
                            <td><span>MT198</span></td>
                            <td><span>Generation</span></td>
                            <td><span class="tag yellow">Backup</span></td>
                        </tr>
                    </table>

                </div>
            </div>

            <!-- Footer -->
            <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>