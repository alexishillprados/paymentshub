<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Content login -->
			<div class="main_login_page">
				<div class="wrap_login">
					<div class="main_content_login">

						<!-- Logo -->
						<span class="logo_ph">
							<img src="images/logo-ph.png">
						</span>

						<!-- Title text -->
						<!-- <p>Complementary text here!</p> -->

						<!-- Form -->
						<div class="form">
							<input type="mail" placeholder="Username" class="mb10">
							<input type="password" placeholder="Password" class="mb20">
							<a href="index.php" class="btn primary">Sign in</a>
							<span class="forgot pt15">Forgot? <a href="reset.php">request reset link</a></span>
						</div>

						<!-- Footer -->
						<div class="footer">
							<span class="text">Don't have an account?</span>
							<a href="" class="btn_start mt10">Get started</a>
						</div>
					</div>
					
					<!-- By Santander -->
					<div class="by_santander">
						<img src="images/by-santander.png">
					</div>
				</div>
			</div>
		</div>

		<!-- No responsive -->
		<div class="main_global_structure_no_responsive">
			<?php include("elements/no-responsive.html") ?>
		</div>
	</body>
</html>



