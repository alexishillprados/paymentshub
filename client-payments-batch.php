<html>
<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Header -->
			<?php include_once("elements/header/primary.html") ?>

			<!-- Menu -->
			<div class="main_header secondary">
				<? $view = "tools"; include_once("elements/header/secondary.html") ?>
			</div>

			<!-- Sub Menu -->
			<div class="main_header third menu_height">
				<? $subview = "client_payments_batch"; include_once("elements/header/third.html") ?>
			</div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0 tabs">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <nav class="main_tabs_header_box_content">
                                    <ul>
                                        <li>Upload MT103</li>
                                        <li>MT103 Job</li>
                                        <li>MT198 Job</li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <!-- Content -->
                    <div class="wrap-tabs">
                        <section>
                            <table>
                                <tr>
                                    <td width="200px"><a class="btn secondary filter">Select a MT103 file</a></td>
                                    <td width="140px">No file selected</td>
                                    <td><a class="btn primary">Submit file</a></td>
                                </tr>
                            </table>
                        </section>
                        <section>
                            <table>
                                <tr>
                                    <th>Job name</th>
                                    <th>Start time</th>
                                    <th>End time</th>
                                    <th>Exit code</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    <td>mt103Job</td>
                                    <td><span>10-10-2017T00:10:20.300</span></td>
                                    <td><span>10-10-2017T00:10:20.300</span></td>
                                    <td><span class="tag red">Failed</span></td>
                                    <td><a class="btn secondary">Re-start</a></td>
                                </tr>
                            </table>
                            <!-- Pagination -->
                            <div class="main_footer_content">
                                <div class="left_content"></div>
                                <div class="right_content">
                                    <div class="main_paginator">
                                        <span class="text"><b>1-3</b> of <b>3</b></span>
                                        <span class="buttons_direction">
									<span class="btn secondary"><span class="icon_20 pag_left"></span></span>
									<span class="btn secondary"><span class="icon_20 pag_right"></span></span>
								</span>
                                    </div>
                                </div>
                            </div>
                            <table>
                                <tr>
                                    <td width="480px">Send accepted / conditionally accepted inbound instructions to bank</td>
                                    <td><a class="btn primary">Generate MT103 file</a></td>
                                </tr>
                            </table>
                        </section>
                        <section>
                            <table>
                                <tr>
                                    <th>Job name</th>
                                    <th>Start time</th>
                                    <th>End time</th>
                                    <th>Exit code</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    <td>mt198Job</td>
                                    <td><span>10-10-2017T00:10:20.300</span></td>
                                    <td><span>10-10-2017T00:10:20.300</span></td>
                                    <td><span class="tag red">Failed</span></td>
                                    <td><a class="btn secondary">Re-start</a></td>
                                </tr>
                            </table>
                            <!-- Pagination -->
                            <div class="main_footer_content">
                                <div class="left_content"></div>
                                <div class="right_content">
                                    <div class="main_paginator">
                                        <span class="text"><b>1-3</b> of <b>3</b></span>
                                        <span class="buttons_direction">
									<span class="btn secondary"><span class="icon_20 pag_left"></span></span>
									<span class="btn secondary"><span class="icon_20 pag_right"></span></span>
								</span>
                                    </div>
                                </div>
                            </div>
                            <table>
                                <tr>
                                    <td width="410px">Send invalid / rejected outbound instructions to bank</td>
                                    <td><a class="btn primary">Generate MT198 file</a></td>
                                </tr>
                            </table>
                        </section>
                    </div>
                </div>
            </div>
		</div>

        <!-- Footer -->
        <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>