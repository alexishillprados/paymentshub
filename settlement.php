<html>
    <head>
        <?php include_once("elements/meta.html") ?>
    </head>
    <body>

        <div class="main_global_structure">

            <!-- Header -->
            <?php include_once("elements/header/primary.html") ?>

            <!-- Menu -->
            <div class="main_header secondary menu_height">
                <? $view = "settlement"; include_once("elements/header/secondary.html") ?>
            </div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <article>Cycles list</article>
                            </div>

                            <!-- Right -->
                            <div class="right_content">
                                <div class="section_content">
                                    <a class="btn secondary icon_btn filter" data-toggle="modal" data-modal="filter-popup">Filters</a>
                                </div>

                                <div class="section_content">
                                    <a class="btn secondary icon_btn_only"><span class="icon_20 download"></span></a>
                                </div>
                            </div>

                        </div>
                    </div>



                    <!-- Content -->
                    <table>
                        <tr>
                            <th></th>
                            <th>Scheme</th>
                            <th>Settelement date</th>
                            <th class="right">Balance</th>
                            <th class="right">Debit</th>
                            <th>Cycle</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <td><span class="icon_20 arrow_tr"></span></td>
                            <td><span>Faster Payments</span></td>
                            <td><span>10-10-2017</span></td>
                            <td class="right"><span>£12.671,00</span></td>
                            <td class="right"><span>£3.121,00</span></td>
                            <td><span>003</span></td>
                            <td><span class="tag yellow">Open</span></td>
                        </tr>

                        <tr>
                            <td><span class="icon_20 arrow_tr toggle_detail_row" data-row="1"></span></td>
                            <td><span>Faster Payments</span></td>
                            <td><span>10-10-2017</span></td>
                            <td class="right"><span>£58.712,00</span></td>
                            <td class="right"><span>£12.121,00</span></td>
                            <td><span>002</span></td>
                            <td><span class="tag green">Closed</span></td>
                        </tr>

                        <!-- Detail -->
                        <tr class="detail_row hidden" id="1">
                            <td colspan="7" class="detail_tr_content">

                                <!-- Content detail tr -->
                                <div class="main_content_tr">
                                    <div class="wrap_content_tr">
                                        <div class="box_content_tr">

                                            <!-- Header tr -->
                                            <div class="header_content_tr">

                                                <!-- Left -->
                                                <div class="left_content">
                                                    <span class="info_text lh1_4">
                                                        <span class="title bolder">Pre-settlement and re-pre-settlements</span>
                                                    </span>
                                                </div>
                                            </div>

                                            <!-- Table content tr -->
                                            <table class="mt20">
                                                <tr>
                                                    <th>Pre-settlement timestamp</th>
                                                    <th class="right">Sent</th>
                                                    <th class="right">Sent total</th>
                                                    <th class="right">Received</th>
                                                    <th class="right">Received total</th>
                                                    <th class="right">Balance</th>
                                                </tr>
                                                <tr>
                                                    <td>2012-12-06-T20:52:10.230</td>
                                                    <td class="right">1</td>
                                                    <td class="right"><span>£825,65</span></td>
                                                    <td class="right">0</td>
                                                    <td class="right"><span>£0,00</span></td>
                                                    <td class="right"><span>£825,65</span></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <span class="sep_10"></span>
                                        <div class="box_content_tr">

                                            <!-- Header tr -->
                                            <div class="header_content_tr">

                                                <!-- Left -->
                                                <div class="left_content">
                                                    <span class="info_text lh1_4">
                                                        <span class="title bolder">Total for all pre-settled and re-pre-settled transactions in this cycle</span>
                                                    </span>
                                                </div>
                                            </div>

                                            <!-- Table content tr -->
                                            <table class="mt20">
                                                <tr>
                                                    <th class="right">Sent</th>
                                                    <th class="right">Sent total</th>
                                                    <th class="right">Received</th>
                                                    <th class="right">Received total</th>
                                                    <th class="right">Balance</th>
                                                    <th class="right">Scheme balance</th>
                                                </tr>
                                                <tr>
                                                    <td class="right">1</td>
                                                    <td class="right"><span>£825,65</span></td>
                                                    <td class="right">0</td>
                                                    <td class="right"><span>£0,00</span></td>
                                                    <td class="right"><span>£825,65</span></td>
                                                    <td class="right"><span>£825,65</span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>


                    <!-- Footer -->
                    <div class="main_footer_content">
                        <div class="left_content"></div>
                        <div class="right_content">
                            <div class="main_paginator">
                                <span class="text"><b>1-3</b> of <b>3</b></span>
                                <span class="buttons_direction">
									<span class="btn secondary"><span class="icon_20 pag_left"></span></span>
									<span class="btn secondary"><span class="icon_20 pag_right"></span></span>
								</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>