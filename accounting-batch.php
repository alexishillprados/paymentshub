<html>
<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Header -->
			<?php include_once("elements/header/primary.html") ?>

			<!-- Menu -->
			<div class="main_header secondary">
				<? $view = "tools"; include_once("elements/header/secondary.html") ?>
			</div>

			<!-- Sub Menu -->
			<div class="main_header third menu_height">
				<? $subview = "accounting_batch"; include_once("elements/header/third.html") ?>
			</div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0 tabs">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <nav class="main_tabs_header_box_content">
                                    <ul>
                                        <li>Daily accounting file</li>
                                        <li>IPS file</li>
                                        <li>TLM reconciliation</li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <!-- Content -->
                    <div class="wrap-tabs">
                        <section>
                            <table>
                                <tr>
                                    <td><a class="btn primary">Generate daily accounting file and send to walker via SFTP</a></td>
                                </tr>
                            </table>
                        </section>
                        <section>
                            <table>
                                <tr>
                                    <td><a class="btn primary">Generate IPS file file and send to partenon via SFTP</a></td>
                                </tr>
                            </table>
                        </section>
                        <section>
                            <table>
                                <tr>
                                    <td><a class="btn primary">Generate TLM file file and send via SFTP</a></td>
                                </tr>
                            </table>
                        </section>
                    </div>
                </div>
            </div>
		</div>

        <!-- Footer -->
        <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>