<html>
    <head>
        <?php include_once("elements/meta.html") ?>
    </head>
    <body>

        <div class="main_global_structure">

            <!-- Header -->
            <?php include_once("elements/header/primary.html") ?>

            <!-- Menu -->
            <div class="main_header secondary">
                <? $view = "tools"; include_once("elements/header/secondary.html") ?>
            </div>

            <!-- Sub Menu -->
            <div class="main_header third menu_height">
                <? $subview = "sort_codes"; include_once("elements/header/third.html") ?>
            </div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <article>Upload sort codes file</article>
                            </div>

                        </div>
                    </div>

                    <!-- Table content -->
                    <table>
                        <tr>
                            <td width="200px"><a class="btn secondary filter">Select a eiscd file</a></td>
                            <td width="140px">No file selected</td>
                            <td><a class="btn primary">Submit file</a></td>
                        </tr>
                    </table>

                </div>
            </div>

            <!-- Footer -->
            <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>