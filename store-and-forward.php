<html>
	<head>
		<?php include_once("elements/meta.html") ?>
	</head>
	<body>

		<div class="main_global_structure">

			<!-- Header -->
			<?php include_once("elements/header/primary.html") ?>

			<!-- Menu -->
			<div class="main_header secondary">
				<? $view = "status"; include_once("elements/header/secondary.html") ?>
			</div>

			<!-- Sub Menu -->
			<div class="main_header third menu_height">
				<? $subview = "store_and_forward"; include_once("elements/header/third.html") ?>
			</div>

            <!-- Content -->
            <div class="main_box_content">
                <div class="box_content pt0">

                    <!-- Header -->
                    <div class="main_header_content">
                        <div class="wrap_header_content">

                            <!-- Left -->
                            <div class="left_content">
                                <article>Store and forward</article>
                            </div>

                        </div>
                    </div>

                    <!-- Content -->
                    <table>
                        <tr>
                            <th>Pending instructions</th>
                            <th>In progress instructions</th>
                            <th>Sent instructions</th>
                            <th>Acecpted instructions</th>
                            <th>Rejected instructions</th>
                            <th>Error instructions</th>
                            <th>Suspended sort codes</th>
                        </tr>
                        <tr>
                            <td><span>0</span></td>
                            <td><span>0</span></td>
                            <td><span>0</span></td>
                            <td><span>0</span></td>
                            <td><span>0</span></td>
                            <td><span>0</span></td>
                            <td><span>0</span></td>
                        </tr>
                    </table>

                </div>
            </div>

            <!-- Footer -->
            <?php include("elements/footer.html") ?>
        </div>

        <!-- No responsive -->
        <div class="main_global_structure_no_responsive">
            <?php include("elements/no-responsive.html") ?>
        </div>
    </body>
</html>